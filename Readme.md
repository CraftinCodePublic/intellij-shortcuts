### Ogólne

funkcja | OSX | Windows
:-------------------|:-------------------|:-------------------
podpowiedź funkcji (autocompletion) | `ctrl + space` | `ctrl + space`
podpowiedź IntelliJ do problemu (importy, poprawa błędów) | `alt + enter` | `alt + enter`
zmiana nazwy (we wszystkich miejscach) | `shift + F6` | `shift + F6`
formatowanie kodu | `alt + cmd + shift + L` | `ctrl + alt + L`

### Edycja tekstu

funkcja | OSX | Windows
:-------------------|:-------------------|:-------------------
duplikacja linii | `cmd + d` | `ctrl + d`
usunięcie całej linii | `cmd + y` | `ctrl + y`
zakomentowanie / odkomentowanie kodu | `cmd + /` | `ctrl + /`                   


### Szablony kodu

szablon | OSX | Windows
:-------------------|:-------------------|:-------------------
metoda `main`| `psvm + TAB` | `psvm + TAB`
System.out.println(); | `sout + TAB` | `sout + TAB`
                   

